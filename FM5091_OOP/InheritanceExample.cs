﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5091_OOP
{
    // Animal sits at the top of our inheritance hierarchy
    public class Animal
    {
        // this is a property
        public int Age { get; set; }
        // another property
        public string Name { get; set; }
        // example of overriding a method, ToString
        public override string ToString()
        {
            return Name;
        }
    }

    public class Mammal : Animal
    {

    }

    public class Giraffe : Mammal
    {
        public Giraffe() { } // this is an instance constructor

        public Giraffe(int age) // here we overload the instance constructor
        {
            Age = age; // here we can access Age property because it inherits from Animal
        }
    }

    public class Reptile : Animal
    {

    }

    public class Turtle : Reptile
    {

    }
}
