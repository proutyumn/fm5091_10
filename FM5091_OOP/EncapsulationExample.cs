﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5091_OOP
{
    public class BlackScholesCalculator
    {
        private static double d1(EuropeanOption o)
        {
            return 0d;
        }

        private static double d2(EuropeanOption o)
        {
            return 0d;
        }

        public static double GetPrice(EuropeanOption o)
        {
            return 0d; // use the properties of the instance of the Euro to calc price
        }
    }

    public class EuropeanOption
    {
        public Underlying Underlying { get; set; }
        public double StrikePrice { get; set; }
        public double Rate { get; set; }
        public double Tenor { get; set; }
        public double Volatility { get; set; }
        public bool IsCall { get; set; }
    }

    public class Underlying
    {
        public double Price { get; set; }
    }


    public class TempMeasure
    {
        double TempInC = 0d;

        public double TempInCelsius
        {
            get
            {
                return TempInC;
            }
            set
            {
                TempInC = value;
            }
        }

        public double TempInFahrenheit
        {
            get
            {
                return TempInC * 8.0 / 25.0; // or something...
            }
            set
            {
                TempInC = value * 25.0 / 8.0; // or something...
            }
        }
    }
}
