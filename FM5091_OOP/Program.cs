﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5091_OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            // make an instance of Giraffe and set some properties
            Giraffe Randy = new Giraffe();
            Randy.Name = "Randy the Giraffe";
            Randy.Age = 12;
            
            // make an instance of Giraffe, setting age using the constructor
            Giraffe Debbie = new Giraffe(5);
            Console.WriteLine(Debbie.Age);

            // make an instance of both square and circle, calling GetArea using a method in this class
            Square mysquare = new Square()
            {
                LengthOfSide = 5
            };
            // instance of my circle
            Circle mycircle = new Circle()
            {
                Radius = 5
            };
            // because of polymorphism, we get rational answers for each
            Console.WriteLine("area of a square: " + CalcTheArea(mysquare));
            Console.WriteLine("area of a circle: " + CalcTheArea(mycircle));

            // example from the slides having to do with object comparison
            object o = new object();
            object o2 = new object();
            object o3 = o;
            Console.WriteLine(o.ToString());
            Console.WriteLine(o.Equals(o2));
            Console.WriteLine(o.Equals(o3));
            Console.WriteLine(o.GetType());
            Console.ReadLine();
        }

        static double CalcTheArea(Shape s)
        {
            return s.GetArea();
        }
    }
}
