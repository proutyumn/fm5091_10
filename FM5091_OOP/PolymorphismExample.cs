﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5091_OOP
{
    // the Shape class sits at the top of our Shape hierarchy
    public class Shape
    {
        /// <summary>
        /// Here we implement a simple GetArea that returns 0.0 since no shape is specified.
        /// </summary>
        /// <returns>Since no shape is specified, return 0.0.</returns>
        public virtual double GetArea()
        {
            return 0d;
        }
    }

    public class Square : Shape
    {
        public double LengthOfSide { get; set; }

        /// <summary>
        /// Here we override the GetArea method inherited from Shape with a specific implementation of area calculation for a square.
        /// </summary>
        /// <returns>The length of a side of the square, squared.</returns>
        public override double GetArea()
        {
            return Math.Pow(LengthOfSide, 2);
        }
    }

    public class Circle : Shape
    {
        public double Radius { get; set; }

        /// <summary>
        /// Here we override the GetArea method inherited from Shape with a specific implementation of area calculation for a circle.
        /// </summary>
        /// <returns>The radius of the circle squared, times Pi.</returns>
        public override double GetArea()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }
}
